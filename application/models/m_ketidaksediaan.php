<?php

class M_ketidaksediaan extends CI_Model{
	function tampil_data(){
		$this->db->select('*');
		$this->db->from('kesediaan');
		$this->db->join('guru', 'guru.id_guru = kesediaan.id_guru');
		$this->db->join('waktu', 'waktu.id_waktu = kesediaan.id_waktu');
		$query = $this->db->get();

		return $query->result();
	}

	public function tampil_data_baru()
	{
		$this->db->select('*');
		$this->db->from('ketidaksediaan');
		$this->db->join('guru', 'guru.id_guru = ketidaksediaan.id_guru');
		$this->db->join('waktu', 'waktu.id_waktu = ketidaksediaan.id_waktu');
		$query = $this->db->get();

		return $query->result();
	}

	function getAll() {
		$this->db->select('*');
		$this->db->from('ketidaksediaan');
		$query = $this->db->get();
		return $query->result();
	}

	function cek_data($id_guru,$id_waktu){
		$this->db->select('*');
		$this->db->from('ketidaksediaan');
		$this->db->where('id_guru',$id_guru);
		$this->db->where('id_waktu',$id_waktu);
		$jumlah=$this->db->count_all_results();
		if ($jumlah > 0) {
			return true;
		}else {
			return false;
		}
	}


	function jumlah_data(){
		return $this->db->get('kesediaan')->num_rows();
	}
	//get random mutasi
	function get_random(){
		$this->db->select('id_kesediaan');
		$this->db->from('kesediaan');
		$this->db->order_by('id_kesediaan','RANDOM');
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result()[0]->id_kesediaan;
	}

	function get_data($kesediaan_a,$kesediaan_b){
		$this->db->select('*');
		$this->db->from('kesediaan');
		$this->db->join('waktu', 'waktu.id_waktu = kesediaan.id_waktu');
		$this->db->where('id_kesediaan',$kesediaan_a);
		$this->db->or_where('id_kesediaan',$kesediaan_b);
		$query = $this->db->get();
		return $query->result();
	}
}
