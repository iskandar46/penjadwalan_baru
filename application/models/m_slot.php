<?php

class M_slot extends CI_Model{
	function tampil_data($number,$offset){
		$this->db->select('*');
		$this->db->from('kesediaan');
		$this->db->join('guru', 'guru.id_guru = kesediaan.id_guru');
		$this->db->join('waktu', 'waktu.id_waktu = kesediaan.id_waktu');
		$query = $this->db->get('',$number,$offset);

		return $query->result();
	}

	public function tampil_data_baru()
	{
		$this->db->select('*');
		$this->db->from('kesediaan');
		$this->db->join('guru', 'guru.id_guru = kesediaan.id_guru');
		$this->db->join('waktu', 'waktu.id_waktu = kesediaan.id_waktu');
		$query = $this->db->get();

		return $query->result();
	}

	function getAll() {
		$this->db->select('*');
		$this->db->from('slot_waktu');
		$query = $this->db->get();
		return $query->result();
	}

	function jumlah_data(){
		return $this->db->get('kesediaan')->num_rows();
	}
	//get random mutasi
	function get_random(){
		$this->db->select('id_kesediaan');
		$this->db->from('kesediaan');
		$this->db->order_by('id_kesediaan','RANDOM');
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result()[0]->id_kesediaan;
	}

	function get_data($slot_a,$slot_b){
		$this->db->select('*');
		$this->db->from('slot_waktu');
		$this->db->join('waktu', 'waktu.id_waktu = slot_waktu.id_waktu');
		$this->db->where('id_slot',$slot_a);
		$this->db->or_where('id_slot',$slot_b);
		$query = $this->db->get();
		return $query->result();
	}
}
