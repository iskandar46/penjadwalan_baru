<?php

class M_tugas extends CI_Model{
	function tampil_data(){
		$this->db->select('id_tugas, nama_guru, nama_mapel, nama_kelas, jumlah_jam');
		$this->db->from('tugas');
		$this->db->join('guru', 'guru.id_guru = tugas.id_guru');
		$this->db->join('kelas', 'kelas.id_kelas = tugas.id_kelas');
		$this->db->join('mapel', 'mapel.id_mapel = tugas.id_mapel');
		$query = $this->db->get();

		return $query->result();
	}

	function getAll() {
		$this->db->select('*');
		$this->db->from('tugas');
		$this->db->order_by('id_kelas','ASC');
		$query = $this->db->get();
		return $query->result();
	}

	// function get_data($tugasa,$tugasb){
	// 	$this->db->select('*');
	// 	$this->db->from('tugas');
	// 	$this->db->join('kelas', 'kelas.id_kelas = tugas.id_kelas');
	// 	$this->db->where('id_tugas',$tugasa);
	// 	$this->db->or_where('id_tugas',$tugasb);
	// 	$query = $this->db->get();
	// 	return $query->result();
	// }

	function get_data($tugas_a,$tugas_b){
		$this->db->select('*');
		$this->db->from('tugas');
		// $this->db->join('guru', 'guru.id_guru = tugas.id_guru');
		$this->db->join('kelas', 'kelas.id_kelas = tugas.id_kelas');
		// $this->db->join('mapel', 'mapel.id_mapel = tugas.id_mapel');
		$this->db->where('id_tugas',$tugas_a);
		$this->db->or_where('id_tugas',$tugas_b);
		$query = $this->db->get();
		// var_dump($query->result());
		return $query->result();
	}


	function jumlah_data(){
		return $this->db->get('tugas')->num_rows();
	}
}
