<?php

class M_jadwal extends CI_Model{
	function tampil_data($number,$offset){
		$this->db->select('id_jadwal, nama_guru, nama_mapel, jumlah_jam, nama_kelas, hari_waktu, count_waktu, jam_mulai_waktu, jam_selesai_waktu');
		$this->db->from('jadwal');
		$this->db->join('tugas', 'tugas.id_tugas = jadwal.id_tugas');
		//$this->db->join('waktu', 'waktu.id_waktu = jadwal.id_waktu');
		//$this->db->join('guru', 'guru.id_guru = jadwal.id_jadwal');
		//$this->db->join('kelas', 'kelas.id_kelas = jadwal.id_jadwal');
		$this->db->join('kesediaan', 'kesediaan.id_kesediaan = jadwal.id_kesediaan');
		$this->db->join('waktu', 'waktu.id_waktu = kesediaan.id_waktu');
		$this->db->join('guru', 'guru.id_guru = tugas.id_guru');
		$this->db->join('kelas', 'kelas.id_kelas = tugas.id_kelas');
		$this->db->join('mapel', 'mapel.id_mapel = tugas.id_mapel');
		$query = $this->db->get('',$number,$offset);

		return $query->result();
	}

	function tampil_data_2(){
		$this->db->select('id_jadwal, nama_guru, nama_mapel, jumlah_jam, nama_kelas, hari_waktu, count_waktu, jam_mulai_waktu, jam_selesai_waktu');
		$this->db->from('jadwal');
		$this->db->join('tugas', 'tugas.id_tugas = jadwal.id_tugas');
		//$this->db->join('waktu', 'waktu.id_waktu = jadwal.id_waktu');
		//$this->db->join('guru', 'guru.id_guru = jadwal.id_jadwal');
		//$this->db->join('kelas', 'kelas.id_kelas = jadwal.id_jadwal');
		$this->db->join('slot_waktu', 'slot_waktu.id_slot = jadwal.id_slot');
		$this->db->join('waktu', 'waktu.id_waktu = slot_waktu.id_waktu');
		$this->db->join('guru', 'guru.id_guru = tugas.id_guru');
		$this->db->join('kelas', 'kelas.id_kelas = tugas.id_kelas');
		$this->db->join('mapel', 'mapel.id_mapel = tugas.id_mapel');
		$query = $this->db->get();

		return $query->result();
	}


	function tampil_data_baru(){
		$this->db->select('id_jadwal, nama_guru, nama_mapel, jumlah_jam, nama_kelas, hari_waktu, count_waktu, jam_mulai_waktu, jam_selesai_waktu');
		$this->db->from('jadwal');
		$this->db->join('tugas', 'tugas.id_tugas = jadwal.id_tugas');
		//$this->db->join('waktu', 'waktu.id_waktu = jadwal.id_waktu');
		//$this->db->join('guru', 'guru.id_guru = jadwal.id_jadwal');
		//$this->db->join('kelas', 'kelas.id_kelas = jadwal.id_jadwal');
		$this->db->join('kesediaan', 'kesediaan.id_kesediaan = jadwal.id_kesediaan');
		$this->db->join('waktu', 'waktu.id_waktu = kesediaan.id_waktu');
		$this->db->join('guru', 'guru.id_guru = tugas.id_guru');
		$this->db->join('kelas', 'kelas.id_kelas = tugas.id_kelas');
		$this->db->join('mapel', 'mapel.id_mapel = tugas.id_mapel');
		$query = $this->db->get();

		return $query->result();
	}

	function json() {
		$this->db->select('id_jadwal, nama_guru, nama_mapel, jumlah_jam, nama_kelas, hari_waktu, count_waktu, jam_mulai_waktu, jam_selesai_waktu');
		$this->db->from('jadwal');
		$this->db->join('tugas', 'tugas.id_tugas = jadwal.id_tugas');
		//$this->db->join('waktu', 'waktu.id_waktu = jadwal.id_waktu');
		//$this->db->join('guru', 'guru.id_guru = jadwal.id_jadwal');
		//$this->db->join('kelas', 'kelas.id_kelas = jadwal.id_jadwal');
		$this->db->join('kesediaan', 'kesediaan.id_kesediaan = jadwal.id_kesediaan');
		$this->db->join('waktu', 'waktu.id_waktu = kesediaan.id_waktu');
		$this->db->join('guru', 'guru.id_guru = tugas.id_guru');
		$this->db->join('kelas', 'kelas.id_kelas = tugas.id_kelas');
		$this->db->join('mapel', 'mapel.id_mapel = tugas.id_mapel');
		$this->datatables->add_column('view', '<a href="world/edit/$1">edit</a> | <a href="world/delete/$1">delete</a>', 'ID');
		return $this->datatables->generate();
	}

	function add($data){
		$this->db->insert_batch('jadwal', $data);
	}
	function trunc() {
		$this->db->truncate('jadwal');
	}

	function jumlah_data(){
		return $this->db->get('jadwal')->num_rows();
	}

}
