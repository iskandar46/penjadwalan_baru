<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Log in | User</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?=base_url('assets/bootstrap/css')?>/bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url('assets/css')?>/custom.css">
  <link rel="stylesheet" href="<?=base_url('assets/dist/css')?>/AdminLTE.min.css">
</head>

  <body style="background-color:white">
    <div class="container">
      <form action="<?php echo base_url('login/aksi_login'); ?>" class="form-signin" role="form" method="post">
        <div class="tengah"><img src="<?=base_url('assets/img')?>/trimulia.png" width="250px" /></div>
        <br/>
        <label class="sr-only" for="username">Username</label>
        <input id="username" name="username" class="form-control" placeholder="Username" required="" autofocus="" type="text" autocomplete="off">
        <label class="sr-only" for="password">Password</label>
        <input id="password" name="password" class="form-control" placeholder="Password" required="" type="password" autocomplete="off">
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>
    </div>
  </body>
</html>
