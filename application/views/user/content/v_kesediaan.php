<div class="container-fluid">


<div class="table-responsive">
	<table class="table">
		<thead>
			<tr>
				<th>No.</th>
				<th>Hari</th>
				<th>Waktu</th>
				<th>Jam Ke-</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<?php
				$no = 1;
				foreach($waktu as $w){
		?>
		<tbody>
				<tr>
					<td><?php echo $no++ ?></td>
					<td><?php echo $w->hari_waktu ?></td>
					<td><?php echo $w->jam_mulai_waktu ?> - <?php echo $w->jam_selesai_waktu ?></td>
					<td><?php echo $w->count_waktu ?></td>
					<td><input type="checkbox" class="cek" name="" value=""></td>
				</tr>
		</tbody>
	<?php } ?>

	</table>
	<button type="button" class="btn btn-primary sub" name="button" disabled>Simpan</button>
</div>
</div>

<script>
	$('.cek').change(function() {
		var checked = $('.cek').filter(function() {
			return $(this).is(':checked');
		});
		$('.sub').prop('disabled', function() {
			return (checked.length >= 3) ? false : true;
		});
	});
</script>
