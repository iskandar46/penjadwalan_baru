<link href="<?=base_url('assets/css')?>/maruti-style.css" type="text/css" rel="stylesheet" />

<div class="jumbotron">
  <div class="container text-center">
    <h1>Sistem Penjadwalan</h1>
    <p>SMK Tri Mulia Jakarta</p>
  </div>
</div>

<div class="container-fluid">
<div class="quick-actions_homepage">
 	<ul class="quick-actions">
       <li> <a href="<?=site_url('Loginproc_admin/lihat_jadwal')?>"> <i class="icon-calendar"></i> Jadwal </a> </li>
       <li> <a href="<?=site_url('Loginproc_admin/lihat_mapel')?>"> <i class="icon-book"></i> Daftar Mapel </a> </li>
       <li> <a href="<?=site_url('Loginproc_admin/lihat_guru')?>"> <i class="icon-people"></i> Daftar Guru </a> </li>
       <li> <a href="<?=site_url('Loginproc_admin/lihat_kelas')?>"> <i class="icon-home"></i> Daftar Kelas </a> </li>
       <li> <a href="<?=site_url('Loginproc_admin/lihat_waktu')?>"> <i class="icon-calendar"></i> Daftar Waktu </a> </li>
       <li> <a href="<?=site_url('Loginproc_admin/lihat_kesediaan')?>"> <i class="icon-calendar"></i> Kesediaan Guru </a> </li>
       <li> <a href="<?=site_url('Loginproc_admin/lihat_tugas')?>"> <i class="icon-calendar"></i> Tugas Guru </a> </li>
  </ul>
</div>
</div>
