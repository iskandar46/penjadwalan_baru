<div class="container" style="margin-top: 20px">
    <div class="row">
			<div class="container" style="align-bottom:10px; padding-bottom:10px;">
				<button type="button" class="btn btn-primary">
						<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
				<a href="<?=site_url('Loginproc_admin/tambah_tugas')?>" style="color:white;">Tambah Tugas</a>
				</button>
			</div>
        <div class="col-md-12">
            <table id="table_id" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
									<th>No.</th>
									<th>Nama Guru</th>
									<th>Mata Pelajaran</th>
									<th>Kelas</th>
									<th>Jumlah Jam</th>
									<th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                    <?php
                        $no = 1;
                        foreach($data_tugas as $t){
                    ?>
                        <tr>
													<td><?php echo $no++ ?></td>
													<td><?php echo $t->nama_guru ?></td>
													<td><?php echo $t->nama_mapel ?></td>
													<td><?php echo $t->nama_kelas ?></td>
													<td><?php echo $t->jumlah_jam ?></td>
													<td><div class="btn-group">
										      <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
										      Aksi<span class="caret"></span></button>
										      <ul class="dropdown-menu" role="menu">
														<li><a href="<?=site_url('Loginproc_admin/edit_waktu/'.$t->id_tugas)?>">Edit</a></li>
														<li><a href ="<?=site_url('Loginproc_admin/hapus_waktu/'.$t->id_tugas)?>">Hapus</a></li>
										      </ul>
										    	</div></td>
                        </tr>
                    <?php }?>
              </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
</script>
