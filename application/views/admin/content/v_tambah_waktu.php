<div class="container">
<div class="row" style="padding-top:10px;">
	<h1>Form Tambah Waktu</h1>
	<form class="" action="<?php echo base_url(). 'Loginproc_admin/aksi_tambah_waktu'; ?>" method="post">
		<div class="form-group">
		  <label>Hari</label>
		  <input type="text" class="form-control" name="hari" >
		</div>
		<label>Waktu Mulai</label>
		<div class="input-group clockpicker">
	    <input type="text" class="form-control" name="mulai" >
	    <span class="input-group-addon">
	        <span class="glyphicon glyphicon-time"></span>
	    </span>
		</div>
		<br>
		<label>Waktu Selesai</label>
		<div class="input-group clockpicker">
	    <input type="text" class="form-control" name="selesai">
	    <span class="input-group-addon">
	        <span class="glyphicon glyphicon-time"></span>
	    </span>
		</div>
		<br>
		<div class="form-group">
		  <label>Jam</label>
		  <input type="number" class="form-control" name="jam" >
		</div>
		<button type="submit" class="btn btn-primary">
		Tambahkan
		</button>
	</form>
</div>
</div>
<script type="text/javascript">
    $('.clockpicker').clockpicker();
</script>
