<div class="container" style="margin-top: 20px">
    <div class="row">
			<div class="container" style="align-bottom:10px; padding-bottom:10px;">
				<button type="button" class="btn btn-primary">
						<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
				<a href="<?=site_url('Loginproc_admin/tambah_kesediaan')?>" style="color:white;">Tambah Kesediaan</a>
				</button>
			</div>
        <div class="col-md-12">
            <table id="table_id" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
									<th>No.</th>
									<th>Nama Guru</th>
									<th>Hari</th>
									<th>Jam Ke-</th>
									<th>Waktu</th>
									<th></th>
                </tr>
              </thead>
              <tbody>
                    <?php
                        $no = 1;
                        foreach($data_kesediaan as $k){
                    ?>
                        <tr>
													<td><?php echo $no++ ?></td>
													<td><?php echo $k->nama_guru?></td>
													<td><?php echo $k->hari_waktu ?></td>
													<td><?php echo $k->count_waktu ?></td>
													<td><?php echo $k->jam_mulai_waktu ?> - <?php echo $k->jam_selesai_waktu ?></td>
													<td><div class="btn-group">
													<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
													Aksi<span class="caret"></span></button>
													<ul class="dropdown-menu" role="menu">
														<li><a href="#">Edit</a></li>
														<li><a href="#">Hapus</a></li>
													</ul>
													</div></td>
                        </tr>
                    <?php }?>
              </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
</script>
