<div class="container">
<div class="row" style="padding-top:10px;">
<div class="table-responsive">
	<table class="table">
		<thead>
			<tr>
				<th>No.</th>
				<th>Nama Guru</th>
				<th>Hari</th>
				<th>Jam Ke-</th>
				<th>Waktu</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
		<?php
				$no = $this->uri->segment('3') + 1;
				foreach($tugas as $k){
		?>
				<tr>
					<td><?php echo $no++ ?></td>
					<td><?php echo $k->nama_guru?></td>
					<td><?php echo $k->nama_mapel ?></td>
					<td><?php echo $k->nama_kelas ?></td>
					<td><?php echo $k->jumlah_jam ?></td>
					<td><div class="btn-group">
		      <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
		      Aksi<span class="caret"></span></button>
		      <ul class="dropdown-menu" role="menu">
		        <li><a href="#">Edit</a></li>
		        <li><a href="#">Hapus</a></li>
		      </ul>
		    	</div></td>
				</tr>
	<?php } ?>
</tbody>
	</table>
	<?php echo $this->pagination->create_links();?>
</div>
</div>
</div>
