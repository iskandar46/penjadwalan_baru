<div class="container" style="margin-top: 20px">
    <div class="row">
			<div class="container" style="align-bottom:10px; padding-bottom:10px;">
				<button type="button" class="btn btn-primary">
						<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
				<a href="<?=site_url('Loginproc_admin/tambah_kelas')?>" style="color:white;">Tambah Kelas</a>
				</button>
			</div>
        <div class="col-md-12">
            <table id="table_id" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
									<th>No.</th>
									<th>Mata Pelajaran</th>
					        <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                    <?php
                        $no = 1;
                        foreach($data_kelas as $k){
                    ?>
                        <tr>
													<td><?php echo $no++ ?></td>
													<td><?php echo $k->nama_kelas ?></td>
													<td><div class="btn-group">
										      <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
										      Aksi<span class="caret"></span></button>
										      <ul class="dropdown-menu" role="menu">
														<li><a href="<?=site_url('Loginproc_admin/edit_kelas/'.$k->id_kelas)?>">Edit</a></li>
														<li><a href ="<?=site_url('Loginproc_admin/hapus_kelas/'.$k->id_kelas)?>">Hapus</a></li>
										      </ul>
										    	</div></td>
                        </tr>
                    <?php }?>
              </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
</script>
