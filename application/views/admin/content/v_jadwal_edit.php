<?php $this->benchmark->elapsed_time() ?>
<div class="container">
<div class="row" style="padding-top:10px;">
<div class="table-responsive">
	<table class="table">
		<thead>
			<tr>
				<th>Nama Guru</th>
				<th>Mata Pelajaran</th>
				<th>Kelas</th>
				<th>Jumlah Jam</th>
				<th>Hari</th>
				<th>Jam Ke-</th>
				<th>Waktu</th>
				<th></th>
			</tr>
		</thead>
		<?php
		$no = $this->uri->segment('3') + 1;
				foreach($jadwal as $j){
		?>
		<tbody>
				<tr>
					<td><?php echo $no++ ?></td>
					<td><?php echo $j->nama_guru?></td>
					<td><?php echo $j->nama_mapel?></td>
					<td><?php echo $j->nama_kelas?></td>
					<td><?php echo $j->jumlah_jam?></td>
					<td><?php echo $j->hari_waktu ?></td>
					<td><?php echo $j->count_waktu ?></td>
					<td><?php echo $j->jam_mulai_waktu ?> - <?php echo $j->jam_selesai_waktu ?></td>
					<td><div class="btn-group">
		      <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
		      Aksi<span class="caret"></span></button>
		      <ul class="dropdown-menu" role="menu">
		        <li><a href="#">Edit</a></li>
		        <li><a href="#">Hapus</a></li>
		      </ul>
		    	</div></td>
				</tr>
		</tbody>
	<?php } ?>
	</table>
	<?php echo $this->pagination->create_links();?>
</div>
</div>
</div>
