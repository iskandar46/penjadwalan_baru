<div class="container" style="margin-top: 20px">
    <div class="row">
        <div class="col-md-12">
            <h2 style="text-align: center;margin-bottom: 30px">Data Buku Dengan CodeIgniter & DataTables</h2>
            <table id="table_id" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama Guru</th>
                    <th>Mata Pelajaran</th>
                    <th>Kelas</th>
                    <th>Hari</th>
                    <th>Jam Ke-</th>
                </tr>
              </thead>
              <tbody>
                    <?php
                        $no = 1;
                        foreach($data_buku as $k){
                    ?>
                        <tr>
                            <td><?php echo $no++;?></td>
                            <td><?php echo $k->nama_guru;?></td>
                            <td><?php echo $k->nama_mapel;?></td>
                            <td><?php echo $k->nama_kelas;?></td>
                            <td><?php echo $k->hari_waktu;?></td>
                            <td><?php echo $k->count_waktu;?></td>
                        </tr>
                    <?php }?>

              </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
</script>
</body>
</html>
