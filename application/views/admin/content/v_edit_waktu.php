<div class="container">
<div class="row" style="padding-top:10px;">
	<?php foreach($waktu as $w){ ?>
	<h1>Form Edit Waktu</h1>
	<form class="" action="<?php echo base_url(). 'Loginproc_admin/update_waktu'; ?>" method="post">
		<div class="form-group">
		  <label>Hari</label>
			<input type="hidden" class="form-control" name="id" value="<?php echo $w->id_waktu ?>" >
		  <input type="text" class="form-control" name="hari" value="<?php echo $w->hari_waktu ?>">
		</div>
		<label>Waktu Mulai</label>
		<div class="input-group clockpicker">
	    <input type="text" class="form-control" name="mulai" value="<?php echo $w->jam_mulai_waktu ?>">
	    <span class="input-group-addon">
	        <span class="glyphicon glyphicon-time"></span>
	    </span>
		</div>
		<br>
		<label>Waktu Selesai</label>
		<div class="input-group clockpicker">
	    <input type="text" class="form-control" name="selesai" value="<?php echo $w->jam_selesai_waktu ?>">
	    <span class="input-group-addon">
	        <span class="glyphicon glyphicon-time"></span>
	    </span>
		</div>
		<br>
		<div class="form-group">
		  <label>Jam</label>
		  <input type="number" class="form-control" name="jam" value="<?php echo $w->count_waktu ?>">
		</div>
		<button type="submit" class="btn btn-default">
		Tambahkan
		</button>
	</form>
	<?php } ?>
</div>
</div>
<script type="text/javascript">
    $('.clockpicker').clockpicker();
</script>
