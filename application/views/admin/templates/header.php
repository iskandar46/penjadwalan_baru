<!DOCTYPE html>
<html lang="en">
<head>
 <title>SMK Tri Mulia Jakarta</title>
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link href="<?=base_url('assets/bootstrap/css')?>/bootstrap.min.css" type="text/css" rel="stylesheet" />
 <link href="<?=base_url('assets/bootstrap/css')?>/bootstrap-clockpicker.min.css" type="text/css" rel="stylesheet" />
 <link href="<?=base_url('assets/css')?>/AdminLTE.css" type="text/css" rel="stylesheet" /> <!--maruti admin-->
 <link href="<?=base_url('assets/datatables')?>/datatables.css" rel="stylesheet" />
 <link href="<?=base_url('assets/datatables')?>/datatables.min.css" rel="stylesheet" />
 <script src="<?=base_url('assets/plugins/jQuery')?>/jquery-2.2.3.min.js"></script>
 <script src="<?=base_url('assets/bootstrap/js')?>/bootstrap.min.js"></script>
 <script src="<?=base_url('assets/bootstrap/js')?>/jquery-clockpicker.min.js"></script>
 <script src="<?=site_url('assets/datatables')?>/datatables.min.js"></script>
 <script src="<?=site_url('assets/datatables')?>/datatables.js"></script>
 <style>
    /* Remove the navbar's default margin-bottom and rounded borders */
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }

    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
  </style>
</head>


<body>
  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">SMK Tri Mulia Jakarta</a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav">
          <li class="active"><a href="<?=site_url('Loginproc_admin')?>">Home</a></li>
          <li><a href="#">About</a></li>
          <li><a href="#">Gallery</a></li>
          <li><a href="#">Contact</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li>	<a>Hai, <?php echo $this->session->userdata("nama"); ?></a></li>
          <li><a href="<?php echo base_url('login/logout_admin'); ?>"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
        </ul>
      </div>
    </div>
  </nav>
