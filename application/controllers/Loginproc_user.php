<?php

class Loginproc_user extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('m_waktu');
		if($this->session->userdata('status') != "login"){
			redirect(base_url("login_user"));
		}
	}

	function index(){
		$this->load->view('user/templates/header');
		$this->load->view('user/index');
		$this->load->view('user/templates/footer');
	}

	function lihat_jadwal(){
		$this->load->view('user/templates/header');
		$this->load->view('user/content/v_jadwal');
		$this->load->view('user/templates/footer');
	}

	function kesediaan(){
		$data['waktu'] = $this->m_waktu->tampil_data()->result();
		$this->load->view('user/templates/header');
		$this->load->view('user/content/v_kesediaan',$data);
		$this->load->view('user/templates/footer');


	}
}
