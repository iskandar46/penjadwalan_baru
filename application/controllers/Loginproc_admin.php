<?php
// ini_set('max_execution_time', 0);
// ini_set('memory_limit', '2048M');
class Loginproc_admin extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->helper(array('url'));
		$this->load->model('m_guru');
		$this->load->model('m_mapel');
		$this->load->model('m_kelas');
		$this->load->model('m_waktu');
		$this->load->model('m_kesediaan');
		$this->load->model('m_tugas');
		$this->load->model('m_jadwal');
		$this->load->model('m_slot');
		$this->load->model('m_ketidaksediaan');
		$this->load->library('Datatables');
    $this->load->helper('url');
		if($this->session->userdata('status') != "login"){
			redirect(base_url("login_admin"));
		}
	}

	function index(){
		$this->load->view('admin/templates/header');
		$this->load->view('admin/index');
		$this->load->view('admin/templates/footer');
	}
	function lihat_jadwal(){
		// $data['kelas'] = $this->m_kelas->tampil_data()->result();
		// $data['waktu'] = $this->m_waktu->tampil_data()->result();
		// $data['jadwal'] = $this->m_jadwal->tampil_data_baru();
		// $this->load->view('admin/templates/header');
		// $this->load->view('admin/content/v_jadwal_new',$data);
		// $this->load->view('admin/templates/footer');

		//create data array
		$data = array(
				'title'     => 'Data Jadwal',
				'data_buku' => $this->m_jadwal->tampil_data_2()
		);
		//render view with data
		$this->load->view('admin/templates/header');
		$this->load->view('admin/content/v_form_generate',$data);
		$this->load->view('admin/templates/footer');
	}
	function lihat_jadwal_edit(){
		$this->load->database();
		$jumlah_data = $this->m_jadwal->jumlah_data();
		$this->load->library('pagination');
		$config['base_url'] = base_url().'Loginproc_admin/lihat_kesediaan/';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 10;
		$from = $this->uri->segment(3);
		/**start style pagination**/
		$config['full_tag_open'] = "<ul class='pagination pagination-sm' style='position:relative; top:-25px;'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		/**end style pagination**/
		$this->pagination->initialize($config);
		$data['jadwal'] = $this->m_jadwal->tampil_data($config['per_page'],$from);
		//$data['jadwal'] = $this->m_jadwal->tampil_data();
		$this->load->view('admin/templates/header');
		$this->load->view('admin/content/v_jadwal_edit',$data);
		$this->load->view('admin/templates/footer');
	}

	function bikin() {

		$this->load->database();
		$tugas = $this->m_tugas->getAll();
		$tugasPecah = [];
		foreach ($tugas as $data) {
			for ($i=0; $i < $data->jumlah_jam; $i++) {
				if (!isset($tugasPecah[$data->id_guru])) {
					$tugasPecah[$data->id_guru] = [];
				}
				array_push($tugasPecah[$data->id_guru], $data);
			}
		}
		// echo "<pre>";
		// print_r($tugasPecah);
		// die();

		// foreach ($tugasPecah as $data) {
		// 	$this->m_tugas->addTemp((Array) $data);
		// }
		$kesediaan = $this->m_kesediaan->getAll();
		shuffle($kesediaan);
		$jadwal = []; // id_tugas, id_kesediaan
		$counter = []; // sisa tugas yang blm disatuin

		foreach ($tugasPecah as $id_guru => $data) {
			$counter[$id_guru] = count($tugasPecah[$id_guru]);
		}

		$total = $counter; // jumlah tugas perguru

		foreach ($kesediaan as $data) {
			if($counter[$data->id_guru] != 0) {
				$jj = array(
					0 => $tugasPecah[$data->id_guru][$total[$data->id_guru]-$counter[$data->id_guru]]->id_tugas,
					1 => $data->id_kesediaan
				);
				array_push($jadwal, $jj);
				$counter[$data->id_guru]--;
			}
		}
// echo "<pre>";
// print_r($jadwal);
// die();
		//
		// $this->m_jadwal->trunc($data);
		// $this->m_jadwal->add($jadwal);
		// redirect("Loginproc_admin/generate_jadwal");
		// die();
		//
		$this->m_jadwal->trunc($data);
		shuffle($jadwal);
		echo "<pre>";
		print_r($jadwal);
		die();
		for ($loop=0; $loop < 50; $loop++) {
			$k=[];//kromosom bener
			$b=[];//kromosom bentrok
			$r=[];//nilai random untuk $b
			$cr=[];//kromosom yg dicrossover
			$m=[];//kromosom yg dimutasi
			for ($i=0; $i < count($jadwal) ; $i+=2) {
				if(array_key_exists($i+1, $jadwal)){
					$kesediaan=$this->banding_kesediaan($jadwal[$i][1],$jadwal[$i+1][1]);
					$tugas=$this->banding_tugas($jadwal[$i][0], $jadwal[$i+1][0]);
					$f=1/(1+$kesediaan+$tugas);
					if ($f == 1) {
						$kromosom = array($jadwal[$i], $jadwal[$i+1]);
						array_push($k, $kromosom);
					}else {
						$kromosom = array($jadwal[$i], $jadwal[$i+1]);
						array_push($b, $kromosom);
					}
				}
			}
			// echo "<pre>";
			// print_r($b);
			// die();
			//bangkitkan nilai random
			for ($i=0; $i < count($b); $i++) {
				$v=rand(0,100)/100;
				array_push($r, $v);
			}

			//variabel nilai PC
			$pc=0.8;
			foreach ($r as $key => $value) {
				if ($value<$pc) {
					array_push($cr, $b[$key]);
				}else {
					array_push($m, $b[$key]);
				}
			}


			//crossover
			for ($i=0; $i < count($cr) ; $i+=2) {
				if(array_key_exists($i+1, $cr)){
					//kiri atas dengan kanan bawah
					$temp= $cr[$i][0][1];
					$cr[$i][0][1] = $cr[$i+1][1][1];
					$cr[$i+1][1][1] = $temp;
					//kiri bawah dengan kanan atas
					$temp= $cr[$i][1][1];
					$cr[$i][1][1] = $cr[$i+1][0][1];
					$cr[$i+1][0][1] = $temp;
				}
			}

			//evaluasi hasil crossover

			foreach ($cr as $key => $value) {
				$kesediaan=$this->banding_kesediaan($value[0][1], $value[1][1]);
				$tugas=$this->banding_tugas($value[0][0], $value[1][0]);
				$f=1/(1+$kesediaan+$tugas);
				if ($f==1) {
					array_push($k, $value);
				}else {
					array_push($m, $value);
				}
			}

			$pm=0.37;
			$pg=2*count($m);
			$jm=$pm*$pg;
			$jm=round($jm);
			$hm=[];

			while(count($hm)!=$jm) {
				$r1=rand(0, count($m)-1);
				$r2=rand(0, 1);
				if(!in_array($r1.$r2, $hm)){
					$m[$r1][$r2][1]=$this->m_kesediaan->get_random();
					array_push($hm,$r1.$r2);
				}
			}

			$k=array_merge($k, $m);

			$jadwal=[];
			foreach ($k as $key => $value) {
				if(array_key_exists(0,$value)){
					array_push($jadwal, $value[0]);
				}
				if(array_key_exists(1,$value)){
					array_push($jadwal, $value[1]);
				}
			}
		}

		$jadwal = array_map(function($tag) {
			return array(
				'id_tugas' => $tag[0],
				'id_kesediaan' => $tag[1]
			);
		}, $jadwal);

		$this->m_jadwal->add($jadwal);
		redirect("Loginproc_admin/generate_jadwal");
		// echo "<pre>";
	 	// print_r($jadwal);
		// die();

	}

	function banding_tugas($tugas_a,$tugas_b){
		$this->load->database();
		$state=0;
		if ($tugas_a == $tugas_b) {
			$state+=2;
		}else {
			$tugas = $this->m_tugas->get_data($tugas_a,$tugas_b);
			// $kesediaan = $this->m_kesediaan->get_data($kesediaan_a,$kesediaan_b);
			// var_dump($tugas);
			// die();
			// $tugas = $this->m_tugas->get_data($tugas_a,$tugas_b);
			if ($tugas[0]->nama_kelas == $tugas[1]->nama_kelas) {
				$state+=1;
			}
		}
		return $state;
		//echo $state ? 'bisa' : 'nggk bisa';
	}

	function banding_kesediaan($kesediaan_a,$kesediaan_b){
		$this->load->database();
		// $tugas = $this->m_tugas->get_data($tugasa, $tugasb);
		$state=0;
		if ($kesediaan_a == $kesediaan_b) {
			$state+=2;
		}
		// elseif ($tugas[0]->nama_kelas == $tugas[1]->nama_kelas) {
		// 	$state+=1;
		// }
		else {
			$kesediaan = $this->m_kesediaan->get_data($kesediaan_a, $kesediaan_b);
			if ($kesediaan[0]->hari_waktu == $kesediaan[1]->hari_waktu) {
				$state+=1;
			}
		}
		return $state;
		//echo $state ? 'bisa' : 'nggk bisa';
	}

	function AG(){
		$this->load->database();
		$tugas = $this->m_tugas->getAll();
		$tugasPecah = [];
		foreach ($tugas as $data) {
			for ($i=0; $i < $data->jumlah_jam; $i++) {
				if (!isset($tugasPecah[$data->id_kelas])) {
					$tugasPecah[$data->id_kelas] = [];
				}
				array_push($tugasPecah[$data->id_kelas], $data);
			}
		}
		// echo "<pre>";
		// print_r($tugasPecah);
		// die();

		// foreach ($tugasPecah as $data) {
		// 	$this->m_tugas->addTemp((Array) $data);
		// }
		$slot = $this->m_slot->getAll();
		// echo "<pre>";
		// print_r($slot);
		// die();

		for ($i=0; $i < count($slot)/2; $i++) {
			$pointer[] = $i;
		}
		shuffle($pointer);
		foreach ($pointer as $key) {
			$hasil[] = $slot[$key*2];
			$hasil[] = $slot[$key*2+1];
		}
		// echo "<pre>";
		// print_r($hasil);
		// die();


		$jadwal = []; // id_tugas, id_kesediaan
		$counter = []; // sisa tugas yang blm disatuin

		foreach ($tugasPecah as $id_kelas => $data) {
			$counter[$id_kelas] = count($tugasPecah[$id_kelas]);
		}

		$total = $counter; // jumlah tugas perguru

		foreach ($hasil as $data) {
			if($counter[$data->id_kelas] != 0) {
				$jj = array(
					0 => $tugasPecah[$data->id_kelas][$total[$data->id_kelas]-$counter[$data->id_kelas]]->id_tugas,
					1 => $data->id_slot
				);
				array_push($jadwal, $jj);
				$counter[$data->id_kelas]--;
			}
		}
		// echo "<pre>";
		// print_r($jadwal);
		// die();



		$this->m_jadwal->trunc($data);
		$jadwal = array_map(function($tag) {
			return array(
				'id_tugas' => $tag[0],
				'id_slot' => $tag[1]
			);
		}, $jadwal);

		echo "<pre>";
		print_r($jadwal);
		die();

		$this->m_jadwal->add($jadwal);
		redirect("Loginproc_admin/generate_jadwal_2");

	}

	function AG2(){
		$jadwalbaru=[];
		for ($s=0; $s < 4; $s++) {
				$jadwal = []; // id_tugas, id_slot
				$this->load->database();
				$tugas = $this->m_tugas->getAll();
				// echo "<pre>";
				// print_r($tugas);
				// die();
				$tugasPecah = [];
				foreach ($tugas as $data) {
					for ($i=0; $i < $data->jumlah_jam; $i++) {
						if (!isset($tugasPecah[$data->id_kelas])) {
							$tugasPecah[$data->id_kelas] = [];
						}
						array_push($tugasPecah[$data->id_kelas], $data);
					}
				}
				//
				// echo "<pre>";
				// print_r($tugasPecah);
				// die();

				// foreach ($tugasPecah as $data) {
				// 	$this->m_tugas->addTemp((Array) $data);
				// }
				$slot = $this->m_slot->getAll();
				// echo "<pre>";
				// print_r($slot);
				// die();
				// echo count($slot);
				// die();

				$slot_map1=[];
				foreach ($slot as $slot_baru){
					$slot_map1[$slot_baru->id_kelas][]=$slot_baru;
				}

				//
				// foreach ($slot_map as $key => $value) {
				// 	shuffle($slot_map[$key]);
				// }
				// echo "<pre>";
				// print_r($slot_map1);
				// die();
				foreach ($slot_map1 as $key => $value) {
					for ($i=0; $i < floor(count($value)/2); $i++) {
						$pointer[$key][] = $i;
					}

					shuffle($pointer[$key]);
					foreach ($pointer[$key] as $key1) {
						$slot_map[$key][] = $slot_map1[$key][$key1*2];
						$slot_map[$key][] = $slot_map1[$key][$key1*2+1];
					}
					if(count($slot_map1[$key]) % 2 == 1) {
						$slot_map[$key][] = $slot_map1[$key][count($slot_map1[$key])-1];
					}

				}
				// echo "<pre>";
				// print_r($slot_map);
				// die();

				foreach ($tugasPecah as $key => $value) {
					for ($l=0; $l < count($value) ; $l++) {
						$tugas_a=(array)$value[$l];
						$slot_a=(array)$slot_map[$key][$l];

						$jjjj=array_merge($tugas_a,$slot_a);
						$angka[0]=$jjjj['id_tugas'];
						$angka[1]=$jjjj['id_guru'];
						$angka[2]=$jjjj['id_mapel'];
						$angka[3]=$jjjj['id_kelas'];
						$angka[4]=$jjjj['id_slot'];
						$angka[5]=$jjjj['id_waktu'];
						$jadwal[]=$angka;
					}
				}
					$jadwalbaru[]=$jadwal;
		}

		//
		// echo "<pre>";
		// print_r($jadwalbaru);
		// // // die();
		// echo count($jadwalbaru);


		// ------------------------------------------------------------------------------
		// buat ngacak berpasangan
		// for ($i=0; $i < count($slot)/2; $i++) {
		// 	$pointer[] = $i;
		// }
		// shuffle($pointer);
		// foreach ($pointer as $key) {
		// 	$hasil[] = $slot[$key*2];
		// 	$hasil[] = $slot[$key*2+1];
		// }



		//-------fungsi evaluasi-------
		//------- evaluasi guru dan jam yang sama-------
		$key_bentrok=[];
		for ($k=0; $k < count($jadwalbaru) ; $k++) {
		//evaluasi guru dan waktu yang sama di tabel jadwal
		for ($i=0; $i < count($jadwalbaru[$k]) ; $i++) {
			for ($j=$i+1; $j < count($jadwalbaru[$k]) ; $j++) {
				if ($jadwalbaru[$k][$i][1] == $jadwalbaru[$k][$j][1] && $jadwalbaru[$k][$i][5] == $jadwalbaru[$k][$j][5]) {
							$key_bentrok[$k][$i]=5;
							$key_bentrok[$k][$j]=5;
				}
			}
		}
		// echo "<pre>";
		// print_r($key_bentrok);
		// die();
		//evaluasi guru dan waktu yang sama dengan data di tabel ketidaksediaan
		foreach ($jadwalbaru[$k] as $key => $value) {
				if ($this->m_ketidaksediaan->cek_data($value[1],$value[5])) {
					if (!array_key_exists($key,$key_bentrok)) {
						$key_bentrok[$k][$key]=5;
					} else {
						$key_bentrok[$k][$key]+=5;
					}
				}
			}
		}
		// echo "<pre>";
		// print_r($key_bentrok);
		// die();
		//seleksi orang tua baru (nilai fitness)
		uasort($key_bentrok, function ($a, $b) {
				$aa=1/(1+array_sum($a));
				$bb=1/(1+array_sum($b));
		    if ($aa == $bb) return 0;
		    return ($aa < $bb) ? -1 : 1;
		});

		$sorted = [];
		foreach ($key_bentrok as $key => $value) {
			$sorted[] = $key;
		}

		$jadwal_sort=[$jadwalbaru[$sorted[0]],$jadwalbaru[$sorted[1]]];
		$keybentrok_sort=[$key_bentrok[$sorted[0]],$key_bentrok[$sorted[1]]];
		// echo "<pre>";
		// print_r($keybentrok_sort);
		// die();
		$jadwal_bolong=$jadwal_sort;
		// echo "<pre>";
		// print_r($state);
		// die();
		$jadwal_bentrok=[];//jadwal bentrok sama dengan keynya
		foreach ($keybentrok_sort as $key => $value) {
			foreach ($value as $key1 => $value1) {
				$jadwal_bentrok[$key][$key1]=$jadwal_sort[$key][$key1];
				$value_bentrok[$key][$key1][]=$jadwal_sort[$key][$key1][0];
				$value_bentrok[$key][$key1][]=$jadwal_sort[$key][$key1][1];
				$value_bentrok[$key][$key1][]=$jadwal_sort[$key][$key1][2];
				$value_bentrok[$key][$key1][]=$jadwal_sort[$key][$key1][3];
				// $jadwal_sort[$key][$key1][0]=0;
				// $jadwal_sort[$key][$key1][1]=0;
				// $jadwal_sort[$key][$key1][2]=0;
				unset($jadwal_sort[$key][$key1][0]);
				unset($jadwal_sort[$key][$key1][1]);
				unset($jadwal_sort[$key][$key1][2]);
			}
		}
		//
		// echo "<pre>";
		// print_r($value_bentrok);
		// // echo count($value_bentrok[0]);
		// die();


		//--------------------update saya--------------------
		//
		foreach ($value_bentrok as $key => $value) {
				shuffle($value);
		}

		foreach ($value_bentrok as $data => $key) {
				foreach ($key as $j => $value) {
					$ketemu = false;
					foreach ($jadwal_sort[$data] as $k => $value1) {
							if (($value[3] == $value1[3]) && (count($value) > count($value1)&&!$ketemu)) {
								$jadwal_sort[$data][$k]=array_merge($value,$value1);
								$ketemu = true;
							}
					}
				}
		}


		foreach ($jadwal_sort as $key => $value) {
			foreach ($value as $key1 => $value1) {
				if ($value1[3] == $value1[4]) {
						unset($jadwal_sort[$key][$key1][3]);
						$jadwal_sort[$key][$key1][3]=	$jadwal_sort[$key][$key1][4];
						unset($jadwal_sort[$key][$key1][4]);

						$jadwal_sort[$key][$key1][4]=	$jadwal_sort[$key][$key1][5];
						unset($jadwal_sort[$key][$key1][5]);
						ksort($jadwal_sort[$key][$key1]);
						$jadwal_sort[$key][$key1][5]=	$jadwal_sort[$key][$key1][6];
				}
			}
		}


		//-------fungsi evaluasi-------
		//------- evaluasi guru dan jam yang sama-------
		$bentrok=[];
		for ($k=0; $k < count($jadwal_sort) ; $k++) {
		//evaluasi guru dan waktu yang sama di tabel jadwal
		for ($i=0; $i < count($jadwal_sort[$k]) ; $i++) {
			for ($j=$i+1; $j < count($jadwal_sort[$k]) ; $j++) {
				if ($jadwal_sort[$k][$i][1] == $jadwal_sort[$k][$j][1] && $jadwal_sort[$k][$i][5] == $jadwal_sort[$k][$j][5]) {
							$bentrok[$k][$i]=5;
							$bentrok[$k][$j]=5;
				}
			}
		}
		//
		//evaluasi guru dan waktu yang sama dengan data di tabel ketidaksediaan
		foreach ($jadwal_sort[$k] as $key => $value) {
				if ($this->m_ketidaksediaan->cek_data($value[1],$value[5])) {
					if (!array_key_exists($key,$bentrok)) {
						$bentrok[$k][$key]=5;
					} else {
						$bentrok[$k][$key]+=5;
					}
				}
			}
		}

		// echo "<pre>";
		// // echo count($jadwal_fix);
		// // // echo count($state);
		// print_r($bentrok);
		// die();

		//seleksi orang tua baru (nilai fitness)
		uasort($bentrok, function ($a, $b) {
				$aa=1/(1+array_sum($a));
				$bb=1/(1+array_sum($b));
		    if ($aa == $bb) return 0;
		    return ($aa < $bb) ? -1 : 1;
		});

		$sorted1 = [];
		foreach ($bentrok as $key => $value) {
			$sorted1[] = $key;
		}

		// $jadwalbaru=[$jadwal_sort[$sorted1[0]]];
		// return $jadwalbaru;
		// $jadwalbaru=[$jadwal_sort[$sorted1[0]]];
		$jadwal_fix=$jadwal_sort[$sorted1[0]];

		//--------------------------------------------------------------------------------------------
		$bentrok1=[];
		//
		$bentrok_guru=[];

		//evaluasi guru dan waktu yang sama di tabel jadwal
		for ($i=0; $i < count($jadwal_fix) ; $i++) {
			for ($j=$i+1; $j < count($jadwal_fix) ; $j++) {
				if ($jadwal_fix[$i][1] == $jadwal_fix[$j][1] && $jadwal_fix[$i][5] == $jadwal_fix[$j][5]) {
					$bentrok1[$i]=5;
					$bentrok1[$j]=5;
				}
			}
		}

		foreach ($jadwal_fix as $key => $value) {
				if ($this->m_ketidaksediaan->cek_data($value[1],$value[5])) {
					if (!array_key_exists($key,$bentrok_guru)) {
						$bentrok_guru[$key]=5;
					} else {
						$bentrok_guru[$key]+=5;
					}
				}
			}


			// $pars1=count($bentrok1);
			// $pars2=count($bentrok2);


		echo "<pre>";
		echo "bentrok waktu berjumlah :".count($bentrok1)."<br>";
		echo "bentrok kesediaan guru berjumlah :".count($bentrok_guru)."<br>";
		// print_r($bentrok1);
		echo count($jadwalbaru);
		die();



		// $k = $this->m_ketidaksediaan->getAll();
		// // echo "<pre>";
		// // print_r($k);
		// // die();
		// foreach ($k as $data) {
		// 	$array_baru[]=array(
		// 		0 => $data->id_guru,
		// 		1 => $data->id_waktu
		// 	);
		// }
		//
		// // echo "<pre>";
		// // echo count($state);
		// echo count($state);
		// // print_r($array_baru);
		// die();

		// foreach ($jadwal_fix as $key => $value) {
		// 	foreach ($value as $key1 => $value1) {
		// 		unset($jadwal_fix[$key][$key1][1]);
		// 		unset($jadwal_fix[$key][$key1][2]);
		// 		unset($jadwal_fix[$key][$key1][3]);
		// 		unset($jadwal_fix[$key][$key1][5]);
		// 		$jadwal_siip[]=$jadwal_fix[$key][$key1];
		// 	}
		// }


		// echo "<pre>";
		// // echo count($jadwal_fix);
		// print_r($jadwal_fix);
		// die();

		$this->m_jadwal->trunc($jadwal_fix);


		$jadwal_fix = array_map(function($tag) {
			return array(
				'id_tugas' => $tag[0],
				'id_slot' => $tag[4]
			);
		}, $jadwal_fix);


		$this->m_jadwal->add($jadwal_fix);
		redirect("Loginproc_admin/generate_jadwal_2");
		//create data array






		// if ($value_bentrok[0][0][3] == $jadwal_sort[0][0][3] &&	count($value_bentrok[0][0]) > count($jadwal_sort[0][0])) {
		// 	$jadwal_fix[]=array_merge($value_bentrok[0][0],$jadwal_sort[0][0]);
		// }else {
		// 	echo "data aman";
		// }

		// $UniqueArray = array();
	 	// foreach($jadwal_fix as $key=>$value){   // rebuild your array
		// 	foreach ($value as $key1 => $value1) {
		// 		$array_unik[$key][$key1]=array_unique($value1[0]);
		// 	}
	 	// }

		// for ($i=0; $i < count($jadwal_fix) ; $i++) {
		// 	for ($j=0; $j < count($jadwal_fix[$i]); $j++) {
		// 		array_unique($jadwal);
		// 	}
		// }
		//
		// echo "<pre>";
		// // // echo count($array_baru);
		// // echo count($state);
		// print_r($jadwal_fix);
		die();
		//--------------------update saya--------------------
		//crossover
		$pc=0.4;
		$jumlah_cv=floor($pc*count($jadwal_sort[0]));
		// echo "<pre>";
		// print_r($jumlah_cv);
		// die();
		$crossovered=[];
		while(count($crossovered) <= $jumlah_cv){
			$random = rand(0,count($jadwal_sort[0])-1);
			if (!in_array($random,$crossovered)) {
				$temp=$jadwal_sort[0][$random];
				$jadwal_sort[0][$random] =	$jadwal_sort[1][$random];
				$jadwal_sort[1][$random] = $temp;
				$crossovered[] = $random;
			}
		}
		// echo "<pre>";
		// print_r($cro);
		// die();

		// echo "<pre>";
		// print_r($jadwal_sort);
		// die();

		foreach ($jadwal_sort as $key => $value) {
			foreach ($value as $key1 => $value1) {
				if (in_array("0", $value1)) {
					$array_nol[$key][]=$key1;
				}
			}
		}
		echo "<pre>";
		print_r($array_nol);
		die();

		//mutasi---------------------------------------------------------------------------------------------------
		// $jadwal_grup=[];
		// foreach ($jadwal_sort as $key => $value) {
		// 	foreach ($value as $key1 => $value1) {
		// 		$jadwal_grup[$key][$value1[0]][$key1]=$jadwal_sort[$key][$key1];
		// 	}
		// }
		//
		//
		// foreach ($tugas as $key => $value) {
		// 	for ($i=0; $i < $value->jumlah_jam ; $i++) {
		// 		if (array_key_exists($value->id_tugas,$jadwal_grup[0])) {
		// 			unset($jadwal_grup[0][$value->id_tugas][key($jadwal_grup[0][$value->id_tugas])]);
		// 		}
		// 		if (array_key_exists($value->id_tugas,$jadwal_grup[1])) {
		// 			unset($jadwal_grup[1][$value->id_tugas][key($jadwal_grup[1][$value->id_tugas])]);
		// 		}
		// 	}
		// }
		// //menghapus array yang pas pasangannya
		// foreach ($jadwal_sort as $key => $value) {
		// 	foreach ($value as $key1 => $value1) {
		// 		if (array_key_exists($key1, $jadwal_grup[$key])) {
		// 			if (count($jadwal_grup[$key][$key1]) == 0) {
		// 					unset($jadwal_grup[$key][$key1]);
		// 			}
		// 		}
		// 	}
		// }
		// //menghilangkan nilai nol, nilai bolong
		// unset($jadwal_grup[0][0]);
		// unset($jadwal_grup[1][0]);
		//----------------------------------------------------------------------------------------------
		//regenerasi

		die();
		//------- evaluasi bentrok ketidaksediaan dengan waktu-------
		$k = $this->m_ketidaksediaan->getAll();
		// echo "<pre>";
		// print_r($k);
		// die();
		foreach ($k as $data) {
			$array_baru[]=array(
				0 => $data->id_guru,
				1 => $data->id_waktu
			);
		}
		for ($i=0; $i < count($jadwalbaru) ; $i++) {
			for ($j=0; $j < count($jadwalbaru[$i]) ; $j++) {
				if ($jadwalbaru[$i][$j][5] == $array_baru[0][0] && $jadwalbaru[$i][$j][1] == $array_baru[0][0] ) {
					$bentrok2[]=$jadwalbaru[$i][$j];
				}
			}
		}
		echo "<pre>";
		print_r($array_baru);
		die();
		//-------pindah silang kedua kromosom--------
		//------crossover---------
		//------mutasi (cek jumlah jam pada tugas, bila terpenuhi maka abaikan)----------
		//-----regenerasi-----
		//evaluasi dan perhitungan fitness
	}



	function shuffle_data(){
		$data = [[1,2,3,4,5,6,7,8,9], [1,2,3,4,5,6,7,8,9]];
		// implode(',',$data);
		// print_r($data);
		// die();
		for ($k=0; $k < count($data); $k++) {
			for ($i=0; $i < floor(count($data[$k])/2); $i++) {
				$pointer[$k][] = $i;
			}
			shuffle($pointer[$k]);
		// echo "<pre>";
		// print_r($pointer);
		// die();

		foreach ($pointer[$k] as $key) {
			$hasil[$k][] = $data[$k][$key*2];
			$hasil[$k][] = $data[$k][$key*2+1];
		}
		if(count($data[$k]) % 2 == 1) {
			$hasil[$k][] = $data[$k][count($data[$k])-1];
		}
		}

	echo '<pre>';
	print_r($hasil);
	die();
		// echo $hasil;

	}




	function generate_jadwal(){
		$this->load->database();
		$jumlah_data = $this->m_jadwal->jumlah_data();
		$this->load->library('pagination');
		$config['base_url'] = base_url().'Loginproc_admin/generate_jadwal/';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 100;
		$from = $this->uri->segment(3);
		/**start style pagination**/
		$config['full_tag_open'] = "<ul class='pagination pagination-sm' style='position:relative; top:-25px;'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		/**end style pagination**/
		$this->pagination->initialize($config);
		$data['jadwal'] = $this->m_jadwal->tampil_data($config['per_page'],$from);
		$this->load->view('admin/templates/header');
		$this->load->view('admin/content/v_jadwal_edit',$data);
		$this->load->view('admin/templates/footer');
	}

	function generate_jadwal_2(){
		//create data array
		$data = array(
				'title'     => 'Data Jadwal',
				'data_buku' => $this->m_jadwal->tampil_data_2(),
		);

		//render view with data
		$this->load->view('admin/templates/header');
		$this->load->view('admin/content/v_jadwal_edit2',$data);
		$this->load->view('admin/templates/footer');
	}


	public function index_jadwal()
		 {
				 //create data array
				 $data = array(
						 'title'     => 'Data Jadwal',
						 'data_buku' => $this->m_jadwal->tampil_data_baru()
				 );
				 //render view with data
				 $this->load->view('admin/content/v_new',$data);
		 }

		 public function index_jadwal_2()
				{
						//create data array
						$data = array(
								'title'     => 'Data Jadwal',
								'data_buku' => $this->m_jadwal->tampil_data_2()
						);
						//render view with data
						$this->load->view('admin/templates/header');
						$this->load->view('admin/content/v_new',$data);
						$this->load->view('admin/templates/footer');
				}

		 public function index_kesediaan()
 			 {
 					 //create data array
 					 $data = array(
 							 'title'     => 'Data Jadwal',
 							 'data_buku' => $this->m_kesediaan->tampil_data_baru()
 					 );
 					 //render view with data
 					 $this->load->view('admin/content/v_new2',$data);
 			 }

	function lihat_kesediaan(){
		//create data array
		$data = array(
				'title'     => 'Data Jadwal',
				'data_kesediaan' => $this->m_kesediaan->tampil_data()
		);
		//render view with data
		$this->load->view('admin/templates/header');
		$this->load->view('admin/content/v_kesediaan',$data);
		$this->load->view('admin/templates/footer');
	}
	function lihat_guru(){
		//create data array
		$data = array(
				'title'     => 'Data Jadwal',
				'data_guru' => $this->m_guru->data()
		);
		//render view with data
		$this->load->view('admin/templates/header');
		$this->load->view('admin/content/v_guru',$data);
		$this->load->view('admin/templates/footer');
	}
	function tambah_guru(){
		$this->load->view('admin/templates/header');
		$this->load->view('admin/content/v_tambah_guru');
		$this->load->view('admin/templates/footer');
	}
	function aksi_tambah_guru(){
		$nama_guru = $this->input->post('nama_guru');
		$nip = $this->input->post('nip');
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$data = array(
			'nama_guru' => $nama_guru,
			'nip_guru' => $nip,
			'username' => $username,
			'password' => $password
			);

		$this->m_guru->tambah_data($data,'guru');

		redirect('Loginproc_admin/lihat_guru');
	}
	function hapus_guru($id){
		$where = array('id_guru' => $id);
		$this->m_guru->hapus_data($where,'guru');
		redirect('Loginproc_admin/lihat_guru/');
	}
	function edit_guru($id){
		$where = array('id_guru' => $id);
		$data['guru'] = $this->m_guru->edit_data($where,'guru')->result();
		$this->load->view('admin/templates/header');
		$this->load->view('admin/content/v_edit_guru',$data);
		$this->load->view('admin/templates/footer');
	}
	function update_guru(){
	$id_guru = $this->input->post('id_guru');
	$nama_guru = $this->input->post('nama_guru');
	$nip_guru = $this->input->post('nip_guru');
	$username = $this->input->post('username');
	$password = $this->input->post('password');

	$data = array(
		'nama_guru' => $nama_guru,
		'nip_guru' => $nip_guru,
		'username' => $username,
		'password' => $password
	);

	$where = array(
		'id_guru' => $id_guru
	);

	$this->m_guru->update_data($where,$data,'guru');
	redirect('Loginproc_admin/lihat_guru');
	}
	function lihat_mapel(){
		//create data array
		$data = array(
				'title'     => 'Data Jadwal',
				'data_mapel' => $this->m_mapel->data()
		);
		//render view with data
		$this->load->view('admin/templates/header');
		$this->load->view('admin/content/v_mapel',$data);
		$this->load->view('admin/templates/footer');
	}
	function tambah_mapel(){
		$this->load->view('admin/templates/header');
		$this->load->view('admin/content/v_tambah_mapel');
		$this->load->view('admin/templates/footer');
	}
	function aksi_tambah_mapel(){
		$mapel = $this->input->post('mapel');

		$data = array(
			'nama_mapel' => $mapel
			);

		$this->m_mapel->tambah_data($data,'mapel');

		redirect('Loginproc_admin/lihat_mapel');
	}
	function hapus_mapel($id){
		$where = array('id_mapel' => $id);
		$this->m_mapel->hapus_data($where,'mapel');
		redirect('Loginproc_admin/lihat_mapel/');
	}
	function edit_mapel($id){
		$where = array('id_mapel' => $id);
		$data['mapel'] = $this->m_mapel->edit_data($where,'mapel')->result();
		$this->load->view('admin/templates/header');
		$this->load->view('admin/content/v_edit_mapel',$data);
		$this->load->view('admin/templates/footer');
	}
	function update_mapel(){
	$id = $this->input->post('id');
	$mapel = $this->input->post('mapel');

	$data = array(
		'nama_mapel' => $mapel
	);

	$where = array(
		'id_mapel' => $id
	);

	$this->m_mapel->update_data($where,$data,'mapel');
	redirect('Loginproc_admin/lihat_mapel');
	}

	function lihat_kelas(){
		//create data array
		$data = array(
				'title'     => 'Data Jadwal',
				'data_kelas' => $this->m_kelas->data()
		);
		//render view with data
		$this->load->view('admin/templates/header');
		$this->load->view('admin/content/v_kelas',$data);
		$this->load->view('admin/templates/footer');
	}
	function tambah_kelas(){
		$this->load->view('admin/templates/header');
		$this->load->view('admin/content/v_tambah_kelas');
		$this->load->view('admin/templates/footer');
	}
	function aksi_tambah_kelas(){
		$kelas = $this->input->post('kelas');
		$data = array(
			'nama_kelas' => $kelas
			);

		$this->m_kelas->tambah_data($data,'kelas');

		redirect('Loginproc_admin/lihat_kelas');
	}
	function hapus_kelas($id){
		$where = array('id_kelas' => $id);
		$this->m_waktu->hapus_data($where,'kelas');
		redirect('Loginproc_admin/lihat_kelas/');
	}
	function edit_kelas($id){
		$where = array('id_kelas' => $id);
		$data['kelas'] = $this->m_kelas->edit_data($where,'kelas')->result();
		$this->load->view('admin/templates/header');
		$this->load->view('admin/content/v_edit_kelas',$data);
		$this->load->view('admin/templates/footer');
	}
	function update_kelas(){
		$id = $this->input->post('id');
		$kelas = $this->input->post('kelas');

		$data = array(
			'nama_kelas' => $kelas
			);

		$where = array(
			'id_kelas' => $id
		);

		$this->m_kelas->update_data($where,$data,'kelas');
		redirect('Loginproc_admin/lihat_kelas');
	}
	function lihat_waktu(){
		//create data array
		$data = array(
				'title'     => 'Data Jadwal',
				'data_waktu' => $this->m_waktu->data()
		);
		//render view with data
		$this->load->view('admin/templates/header');
		$this->load->view('admin/content/v_waktu',$data);
		$this->load->view('admin/templates/footer');
	}
	function tambah_waktu(){
		$this->load->view('admin/templates/header');
		$this->load->view('admin/content/v_tambah_waktu');
		$this->load->view('admin/templates/footer');
	}
	function aksi_tambah_waktu(){
		$hari = $this->input->post('hari');
		$mulai = $this->input->post('mulai');
		$selesai = $this->input->post('selesai');
		$jam = $this->input->post('jam');

		$data = array(
			'hari_waktu' => $hari,
			'jam_mulai_waktu' => $mulai,
			'jam_selesai_waktu' => $selesai,
			'count_waktu' => $jam
			);

		$this->m_waktu->tambah_data($data,'waktu');

		redirect('Loginproc_admin/lihat_waktu');
	}
	function hapus_waktu($id){
		$where = array('id_waktu' => $id);
		$this->m_waktu->hapus_data($where,'waktu');
		redirect('Loginproc_admin/lihat_waktu/');
	}
	function edit_waktu($id){
		$where = array('id_waktu' => $id);
		$data['waktu'] = $this->m_waktu->edit_data($where,'waktu')->result();
		$this->load->view('admin/templates/header');
		$this->load->view('admin/content/v_edit_waktu',$data);
		$this->load->view('admin/templates/footer');
	}
	function update_waktu(){
		$id = $this->input->post('id');
		$hari = $this->input->post('hari');
		$mulai = $this->input->post('mulai');
		$selesai = $this->input->post('selesai');
		$jam = $this->input->post('jam');

		$data = array(
			'hari_waktu' => $hari,
			'jam_mulai_waktu' => $mulai,
			'jam_selesai_waktu' => $selesai,
			'count_waktu' => $jam
			);
		$where = array(
			'id_waktu' => $id
		);

		$this->m_waktu->update_data($where,$data,'waktu');
		redirect('Loginproc_admin/lihat_waktu');
	}

	function lihat_tugas(){
		//create data array
		$data = array(
				'title'     => 'Data Jadwal',
				'data_tugas' => $this->m_tugas->tampil_data()
		);
		//render view with data
		$this->load->view('admin/templates/header');
		$this->load->view('admin/content/v_tugas',$data);
		$this->load->view('admin/templates/footer');
	}
}
